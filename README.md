```
conda remove --name p36 --all
conda create --name p36 python=3.6
conda activate p36
pip install keras
pip install tensorflow
# downgrade 2.0->1.14
pip install tensorflow==1.14
pip install Pillow
pip install matplotlib
pip install sklearn
```
